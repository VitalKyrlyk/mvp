package ex.a.main;


import dagger.Provides;
import ex.a.base.BasePresenter;

import java.util.Random;

public class MainPresenter extends BasePresenter<MainContract.View> implements MainContract.Presenter{
    private String[] helloTexts = {"BONJOUR", "HOLA", "HALLO", "MERHABA", "HELLO", "CIAO", "KONNICHIWA"};

    @Override
    public void loadHelloText() {
        Random random = new Random();
        String hello = helloTexts[random.nextInt(helloTexts.length)];
        getView().onTextLoaded(hello);
    }
}
