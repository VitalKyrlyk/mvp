package ex.a.main;

import ex.a.base.BaseMvpPresenter;
import ex.a.base.BaseView;

public interface MainContract {
    // User actions. Presenter will implement
    interface Presenter extends BaseMvpPresenter<View> {
        void loadHelloText();
    }

    // Action callbacks. Activity/Fragment will implement
    interface View extends BaseView {
        void onTextLoaded(String text);
    }
}
