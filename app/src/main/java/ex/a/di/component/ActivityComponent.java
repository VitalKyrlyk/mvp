package ex.a.di.component;

import ex.a.di.module.ActivityModule;
import ex.a.main.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton

@Component(modules = {ActivityModule.class})

public interface ActivityComponent {

    void inject(MainActivity obj);



}